package com.cherri.simpletx.model.dao;

import com.cherri.simpletx.dto.IssuerQueryDTO;
import com.cherri.simpletx.exception.OceanException;
import com.cherri.simpletx.model.entity.Issuer;
import com.cherri.simpletx.model.enumerator.ErrorCode;
import com.cherri.simpletx.model.repository.IssuerRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Log4j2
@Repository
public class IssuerDAO {
    private final IssuerRepository repo;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public IssuerDAO(IssuerRepository repo, NamedParameterJdbcTemplate jdbcTemplate) {
        this.repo = repo;
        this.jdbcTemplate = jdbcTemplate;
    }

    public Issuer add(Issuer issuer) {
        issuer.setCreateMills(new Date().getTime());
        issuer.setUpdateMills(new Date().getTime());
        return repo.save(issuer);
    }

    public Issuer update(Issuer issuer) {
        if (issuer == null) {
            throw new IllegalArgumentException(
                    "Failed in update issuer due to missing issuer content.");
        }
        Optional<Issuer> tgtContent = repo.findUnDeleteById(issuer.getId());

        if (!tgtContent.isPresent()) {
            log.error(
                    "Failed in update issuers due to unknown query content with id: " + issuer.getId());
            throw new OceanException(ErrorCode.ISSUER_NOT_FOUND.getCode(), ErrorCode.ISSUER_NOT_FOUND.getMessage());
        }
        if (!CollectionUtils.isEmpty(repo.findByBankCodeAndIdIsNot(issuer.getBankCode(), issuer.getId()))) {
            throw new OceanException(ErrorCode.BANK_CODE_DUPLICATED.getCode(), ErrorCode.BANK_CODE_DUPLICATED.getMessage());
        }
        Issuer tgtIssuer = tgtContent.get();
        tgtIssuer.setName(issuer.getName());
        tgtIssuer.setBankCode(issuer.getBankCode());
        tgtIssuer.setUpdateMills(new Date().getTime());
        return repo.save(tgtIssuer);
    }

    public boolean delete(Long id) {
        Optional<Issuer> tgtContent = repo.findById(id);
        if (!tgtContent.isPresent()) {
            return false;
        }

        Issuer tgtIssuer = tgtContent.get();
        tgtIssuer.setDeleteFlag(true);
        repo.save(tgtIssuer);

        return true;
    }

    public List<Issuer> getListByQueryDTO(IssuerQueryDTO issuerQueryDTO) {

        if (issuerQueryDTO == null) {
            throw new IllegalArgumentException(
                    "Failed in list issuers due to missing query content.");
        }

        // 更早就應該
        if (issuerQueryDTO.getIssuer_id() != null) {
            final Long issuer_id = issuerQueryDTO.getIssuer_id();
            final Optional<Issuer> byId = repo.findUnDeleteById(issuer_id);
            if (!byId.isPresent()) {
                log.error(
                        "Failed in list issuers due to unknown query content with id: " + issuerQueryDTO.getIssuer_id());
                throw new OceanException(ErrorCode.ISSUER_NOT_FOUND.getCode(), ErrorCode.ISSUER_NOT_FOUND.getMessage());
            } else {
                final List issuer_list = new ArrayList();
                issuer_list.add(byId.get());
                return issuer_list;
            }
        } else {
            return repo.findAllUnDelete();
        }
    }

    public Optional<Issuer> getById(Long id) {
        return repo.findById(id);
    }

    public Optional<Issuer> getUnDeleteById(Long id) {
        return repo.findUnDeleteById(id);
    }

    public List<Issuer> getByBankCode(String bankCode) {
        return repo.findByBankCodeAndDeleteFlag(bankCode, Boolean.FALSE);
    }

}
