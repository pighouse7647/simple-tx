package com.cherri.simpletx.model.dao;

import com.cherri.simpletx.dto.TransactionQueryDTO;
import com.cherri.simpletx.model.entity.Transaction;
import com.cherri.simpletx.model.repository.TransactionRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Log4j2
@Repository
public class TransactionDAO {

    private TransactionRepository repo;

    public TransactionDAO(TransactionRepository repo) {
        this.repo = repo;
    }


    public Transaction pay(Transaction transaction) {
        return repo.save(transaction);
    }

    public Optional<Page<Transaction>> queryListByQueryDTO(TransactionQueryDTO transactionQueryDTO) {

        final Pageable pageRequest =
                PageRequest.of(
                        transactionQueryDTO.getPage() - 1, transactionQueryDTO.getRecords_per_page(),
                        Sort.Direction.ASC, "id");

        Page<Transaction> page = repo.findAll((root, query, criteriaBuilder) -> {
            final List<Predicate> predList = new ArrayList<>();

            if (transactionQueryDTO.getAmount() != null) {
                if (transactionQueryDTO.getAmount().getMaximum() == null || transactionQueryDTO.getAmount().getMinimum() == null) {
                    log.error("Required if amount is given");
                } else {
                    // Add criteria(amount)
                    predList.add(criteriaBuilder.greaterThanOrEqualTo(root.get("amount"), transactionQueryDTO.getAmount().getMinimum()));
                    predList.add(criteriaBuilder.lessThanOrEqualTo(root.get("amount"), transactionQueryDTO.getAmount().getMaximum()));
                }
            }
            if (transactionQueryDTO.getIssuer_id() != null) {
                // Add criteria(issuer_id)
                predList.add(criteriaBuilder.equal(root.get("issuerId"), transactionQueryDTO.getIssuer_id()));
            }
            if (transactionQueryDTO.getTrans_id() != null) {
                // Add criteria(trans_id)
                predList.add(criteriaBuilder.equal(root.get("transId"), transactionQueryDTO.getTrans_id()));
            }

            Predicate[] predicates = new Predicate[predList.size()];
            return criteriaBuilder.and(predList.toArray(predicates));
        }, pageRequest);

        return Optional.of(page);
    }

    public boolean updateTimeRecord(Transaction transaction) {
        Optional<Transaction> tgtOpt = repo.findById(transaction.getId());
        if (!tgtOpt.isPresent()) {
            return false;
        }
        final Transaction tgtContent = tgtOpt.get();
        tgtContent.setStartMillis(transaction.getStartMillis());
        tgtContent.setEndMillis(transaction.getEndMillis());
        final Transaction result = repo.save(tgtContent);
        return result != null && result.getId() > 0;
    }
}

