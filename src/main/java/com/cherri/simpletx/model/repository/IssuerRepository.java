package com.cherri.simpletx.model.repository;

import com.cherri.simpletx.model.entity.Issuer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IssuerRepository extends
        CrudRepository<Issuer, Long> {

    @Query(value = "select * from ISSUER where DELETE_FLAG=0", nativeQuery = true)
    List<Issuer> findAllUnDelete();

    @Query(
            value = "select * from ISSUER where DELETE_FLAG=0 and ID=?1", nativeQuery = true)
    Optional<Issuer> findUnDeleteById(Long id);

    List<Issuer> findByBankCode(String bankCode);

    List<Issuer> findByBankCodeAndIdIsNot(String bankCode, Long id);

    List<Issuer> findByBankCodeAndDeleteFlag(String bankCode, Boolean deleteFlag);


}
