package com.cherri.simpletx.model.entity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;

@Converter
public class BooleanToBigDecimalConverter implements AttributeConverter<Boolean, BigDecimal> {

    @Override
    public BigDecimal convertToDatabaseColumn(Boolean attribute) {
        return Boolean.TRUE.equals(attribute) ? BigDecimal.ONE : BigDecimal.ZERO;
    }

    @Override
    public Boolean convertToEntityAttribute(BigDecimal dbData) {
        return BigDecimal.ONE.equals(dbData) ? true : false;
    }
}
