package com.cherri.simpletx.model.entity;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Slf4j
@Entity
@ToString
@Table(name = "TRANSACTION")
public class Transaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(
            name = "TRANSACTION_ID_GENERATOR",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {@Parameter(name = "sequence_name", value = "TRANSACTION_ID_SEQ")})
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TRANSACTION_ID_GENERATOR")
    @Column(name = "ID")
    private Long id;

    //    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
//    @JoinColumn(name = "ISSUER_ID", nullable = false)
//    private Issuer issuer;

    @Column(name = "ISSUER_ID")
    private Long issuerId;

    @Column(name = "TRANS_ID")
    private String transId;

    @Column(name = "MASKED_ACCT_NUMBER")
    private String maskedAcctNumber;

    @Column(name = "AMOUNT")
    private Integer amount;

    @Column(name = "START_MILLIS")
    private Long startMillis;

    @Column(name = "END_MILLIS")
    private Long endMillis;

}