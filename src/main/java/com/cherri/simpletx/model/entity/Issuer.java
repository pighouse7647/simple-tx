package com.cherri.simpletx.model.entity;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Slf4j
@Entity
@ToString
@Table(name = "ISSUER")
public class Issuer implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GenericGenerator(
            name = "ISSUER_ID_GENERATOR",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {@Parameter(name = "sequence_name", value = "ISSUER_ID_SEQ")})
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ISSUER_ID_GENERATOR")
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "BANK_CODE")
    private String bankCode;

    @Column(name = "CREATE_MILLS")
    private Long createMills;

    @Column(name = "UPDATE_MILLS")
    private Long updateMills;

    @Column(name = "DELETE_FLAG")
    private Boolean deleteFlag = false;

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "issuer")
//    private List<Transaction> transactions = new ArrayList<>();

}