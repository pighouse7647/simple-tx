package com.cherri.simpletx.model.enumerator;

import lombok.Getter;

@Getter
public enum ErrorCode {

    SUCCESS(0, "Success"),
    FAILED(1, "Failed"),
    EXCEPTION_HAPPENED(2, "Exception happened"),
    REQUEST_PARAMS_WRONG_FORMAT(3, "Request params wrong format"),
    BANK_CODE_DUPLICATED(4, "bank_code duplicated"),
    ISSUER_NOT_FOUND(5, "issuer not found"),
    CONNECTION_ERROR(6, "connection error"),
    JSON_PARSE_ERROR(7, "json parse error")

    ;


    private Integer code;
    private String message;

    ErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public static ErrorCode codeOf(Integer code) {
        ErrorCode[] reasonList = values();
        for (ErrorCode err : reasonList) {
            if (err.code.equals(code)) {
                return err;
            }
        }
        return null;
    }
}
