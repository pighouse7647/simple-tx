package com.cherri.simpletx.controller;

import com.cherri.simpletx.aspect.AuditLogHandler;
import com.cherri.simpletx.aspect.AuditLogMethodNameEnum;
import com.cherri.simpletx.dto.ApiIssuerResponse;
import com.cherri.simpletx.dto.ApiResponse;
import com.cherri.simpletx.dto.IssuerDTO;
import com.cherri.simpletx.dto.IssuerQueryDTO;
import com.cherri.simpletx.exception.OceanException;
import com.cherri.simpletx.facade.IssuerFacade;
import com.cherri.simpletx.filter.APIExceptionAdvice;
import com.cherri.simpletx.model.enumerator.ErrorCode;
import com.cherri.simpletx.model.enumerator.ResultStatus;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("/issuer")
public class IssuerController {

    @Autowired
    private IssuerFacade issuerFacade;

    @PostMapping("/create")
    @AuditLogHandler(methodName = AuditLogMethodNameEnum.ISSUER_CREATE)
    @ApiImplicitParams(@ApiImplicitParam(name = "issuerDTO", dataType = "IssuerDTO"))
    public ApiResponse addIssuer(@RequestBody IssuerDTO issuerDTO) {
        try {
            issuerFacade.add(issuerDTO);
        } catch (OceanException e) {
            ErrorCode errorCode = ErrorCode.codeOf(e.getErrCode());
            log.error("Failed in add issuer, please contact system administrator. , Code={}, Message={}",
                    errorCode.getCode(), errorCode.getMessage(), e);
            return new ApiResponse(errorCode);
        } catch (Exception e) {
            ErrorCode errorCode = ErrorCode.EXCEPTION_HAPPENED;
            log.error("Failed in add issuer, please contact system administrator. , Code={}, Message={}",
                    errorCode.getCode(), errorCode.getMessage(), e);
        }
        return ApiResponse.SUCCESS_API_RESPONSE;
    }

    @PostMapping("/list")
    @AuditLogHandler(methodName = AuditLogMethodNameEnum.ISSUER_LIST)
    @ApiImplicitParams(@ApiImplicitParam(name = "issuerQueryDTO", dataType = "IssuerQueryDTO"))
    public ApiResponse listIssuer(@RequestBody IssuerQueryDTO issuerQueryDTO) {
        final List<IssuerDTO> result = issuerFacade.queryListByQueryDTO(issuerQueryDTO);
        if (result == null) {
            return new ApiResponse(ResultStatus.SERVER_ERROR, "Failed in query issuers.");
        } else {
            return ApiIssuerResponse.valueOf(result);
        }
    }

    @PostMapping("/update")
    @AuditLogHandler(methodName = AuditLogMethodNameEnum.ISSUER_UPDATE)
    @ApiImplicitParams(@ApiImplicitParam(name = "issuerDTO", dataType = "IssuerDTO"))
    public ApiResponse updateIssuer(@RequestBody IssuerDTO issuerDTO) {
        boolean addSuccess = issuerFacade.update(issuerDTO);
        if (addSuccess) {
            return ApiResponse.SUCCESS_API_RESPONSE;
        } else {
            return new ApiResponse(
                    ResultStatus.SERVER_ERROR,
                    "Failed in update issuer, please contact system administrator.");
        }
    }

    @PostMapping("/delete")
    @AuditLogHandler(methodName = AuditLogMethodNameEnum.ISSUER_DELETE)
    @ApiImplicitParams(@ApiImplicitParam(name = "issuerDTO", dataType = "IssuerDTO"))
    public ApiResponse deleteIssuer(@RequestBody IssuerDTO issuerDTO) {
        boolean deleteSuccess = issuerFacade.delete(issuerDTO);
        if (deleteSuccess) {
            return ApiResponse.SUCCESS_API_RESPONSE;
        } else {
            return new ApiResponse(
                    ResultStatus.SERVER_ERROR,
                    "Failed in delete issuer, please contact system administrator.");
        }
    }


}
