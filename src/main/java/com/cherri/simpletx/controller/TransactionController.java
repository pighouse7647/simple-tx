package com.cherri.simpletx.controller;

import com.cherri.simpletx.aspect.AuditLogHandler;
import com.cherri.simpletx.aspect.AuditLogMethodNameEnum;
import com.cherri.simpletx.dto.*;
import com.cherri.simpletx.facade.TransactionFacade;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@Log4j2
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionFacade transactionFacade;

    @PostMapping("/pay")
    @AuditLogHandler(methodName = AuditLogMethodNameEnum.TRANSACTION_PAY)
    @ApiImplicitParams(@ApiImplicitParam(name = "payDTO", dataType = "TransactionPayDTO"))
    public ApiResponse payTransaction(@Validated @RequestBody TransactionPayDTO payDTO, BindingResult bindingResult) throws NoSuchMethodException, MethodArgumentNotValidException {
        if (bindingResult.hasErrors()) {
            throw new MethodArgumentNotValidException(new MethodParameter(TransactionController.class.getMethod(
                    "payTransaction", TransactionPayDTO.class, BindingResult.class), 0), bindingResult);
        }
        return new ApiTransactionPayResponse(transactionFacade.pay(payDTO));
    }

    @PostMapping("/query")
    @AuditLogHandler(methodName = AuditLogMethodNameEnum.TRANSACTION_QUERY)
    @ApiImplicitParams(@ApiImplicitParam(name = "queryDTO", dataType = "TransactionQueryDTO"))
    public ApiPagesResponse queryTransaction(@RequestBody TransactionQueryDTO queryDTO) {
        Optional<PagingResultsDTO<List<TransactionDTO>>> result = transactionFacade.query(queryDTO);
        if (result.isPresent()) {
            return new ApiIssuerPagesResponse(result.get());
        }
        return new ApiPagesResponse(new PagingResultsDTO());
    }

}
