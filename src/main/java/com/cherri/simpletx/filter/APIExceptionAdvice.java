package com.cherri.simpletx.filter;

import com.cherri.simpletx.dto.ApiResponse;
import com.cherri.simpletx.exception.OceanException;
import com.cherri.simpletx.model.enumerator.ResultStatus;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@ControllerAdvice(basePackages = "com.cherri.simpletx.controller.**")
public class APIExceptionAdvice extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<ObjectError> errors = ex.getBindingResult().getAllErrors();
        String errorMessages =
                errors.stream()
                        .map(
                                error -> {
                                    String objName = error.getObjectName();
                                    if (error instanceof FieldError) {
                                        String fieldName = ((FieldError) error).getField();
                                        return String.format(
                                                "[%s.%s] %s", objName, fieldName, error.getDefaultMessage());
                                    }
                                    return error.getDefaultMessage();
                                })
                        .collect(Collectors.joining("; "));

        return new ResponseEntity<>(
                new ApiResponse(ResultStatus.COLUMN_NOT_EMPTY, errorMessages), HttpStatus.OK);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<ApiResponse> constraintViolationExceptionHandler(Exception e) {
        log.error(e.getMessage(), e);

        return new ResponseEntity<>(
                new ApiResponse(ResultStatus.NOT_SUPPORTED, e.getMessage()), HttpStatus.OK);
    }

    @ExceptionHandler({OceanException.class})
    public ResponseEntity<ApiResponse> oceanExceptionHandler(OceanException re) {
        log.error(re.getMessage(), re);
        ResultStatus errStatus =
                re.getErrCode() == null ? ResultStatus.SERVER_ERROR : ResultStatus.codeOf("" + re.getErrCode());
        return new ResponseEntity<>(new ApiResponse(errStatus, re.getMessage()), HttpStatus.OK);
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<ApiResponse> accessDeniedHandler(AccessDeniedException re) {
        return new ResponseEntity<>(
                new ApiResponse(ResultStatus.FORBIDDEN, "You don't have permission"), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ApiResponse> exceptionHandler(Exception e) {
        log.error(e.getMessage(), e);

        return new ResponseEntity<>(
                new ApiResponse(ResultStatus.SERVER_ERROR, e.getMessage()), HttpStatus.OK);
    }
}
