package com.cherri.simpletx.exception;

import com.cherri.simpletx.model.enumerator.ErrorCode;

public class OceanException extends RuntimeException {

  protected String status = "";
  protected Integer errCode;
  // 外部顯示訊息
  protected String errMsg = "";

  protected String messageType = "";

  public OceanException() {
    super();
  }

  public OceanException(ErrorCode errorCode) {
    super();
    this.errCode = errorCode.getCode();
    this.errMsg = errorCode.getMessage();
  }
  public OceanException(String message) {
    super(message);
    this.errMsg = message;
  }

  public OceanException(String message, Throwable cause) {
    super(message, cause);
    this.errMsg = message;
  }

  public OceanException(Integer errCode, String message) {
    super(message);
    this.errCode = errCode;
    this.errMsg = message;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Integer getErrCode() {
    return errCode;
  }

  public void setErrCode(Integer errCode) {
    this.errCode = errCode;
  }

  public String getErrMsg() {
    return errMsg;
  }

  public void setErrMsg(String errMsg) {
    this.errMsg = errMsg;
  }

  public String getMessageType() {
    return messageType;
  }

  public void setMessageType(String messageType) {
    this.messageType = messageType;
  }
}
