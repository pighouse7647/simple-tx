package com.cherri.simpletx.util;

import com.cherri.simpletx.dto.InternalHttpRequestParameter;
import com.cherri.simpletx.exception.OceanException;
import com.cherri.simpletx.model.enumerator.ErrorCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Log4j2
@Component
public class InternalHttpConnector {

    public static final String PROTOCOL_VERSION = "TLSv1.2";

    private static final MediaType APPLICATION_JSON =
            MediaType.parse("application/json; charset=utf-8");

    private final ObjectMapper objectMapper;
    private static OkHttpClient client;

    final TrustManager[] trustAllCerts =
            new TrustManager[]{
                    new X509TrustManager() {

                        @Override
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] chain, String authType)
                                throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] chain, String authType)
                                throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

    @Autowired
    public InternalHttpConnector(Environment environment, ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;

        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(OKHttpUtil.MAX_REQUESTS);
        dispatcher.setMaxRequestsPerHost(OKHttpUtil.MAX_REQUESTS_PER_HOST);

        int maxIdle = Integer.parseInt(environment.getProperty("okhttp.max.idle.connections", "10"));
        int keepAlive = Integer.parseInt(environment.getProperty("okhttp.keep.alive.duration", "5"));

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance(PROTOCOL_VERSION);
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            log.error("[Error][Initializing SSLContext failed], code={}, message={}", ErrorCode.FAILED.getCode(), ErrorCode.FAILED.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            log.error("[Error][Getting Instance of SSLContext failed], code={}, message={}", ErrorCode.FAILED.getCode(), ErrorCode.FAILED.getMessage(), e);
            throw new RuntimeException(e);
        }

        SSLSocketFactory sslSocketFactory = sc.getSocketFactory();

        this.client =
                new OkHttpClient.Builder()
                        .retryOnConnectionFailure(true)
                        .connectionPool(new ConnectionPool(maxIdle, keepAlive, TimeUnit.MINUTES))
                        .connectTimeout(OKHttpUtil.DEFAULT_CONNECT_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(OKHttpUtil.DEFAULT_READ_TIMEOUT, TimeUnit.SECONDS)
                        .dispatcher(dispatcher)
                        .hostnameVerifier(InternalHttpConnector::verify)
                        .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                        .build();
    }

    private static boolean verify(String s, SSLSession sslSession) {
        // Trust all hostname
        return true;
    }

    public Optional<Object> get(String url, Class resDtoClz) throws OceanException {
        Request request = new Request.Builder().url(url).get().build();
        String result = getResult(request);
        return parseToDto(resDtoClz, result, url);
    }

    public Optional<Object> post(InternalHttpRequestParameter reqParam) throws OceanException {
        RequestBody body = RequestBody.create(APPLICATION_JSON, reqParam.getReqJson());
        Request request = new Request.Builder().url(reqParam.getUrl()).post(body).build();
        String result = getResult(request);
        return parseToDto(reqParam.getResDtoClz(), result, reqParam.getUrl());
    }

    public Optional<Object> uploadFileInForm(
            InternalHttpRequestParameter reqParam, MultipartFile uploadedFile) throws OceanException {
        RequestBody requestBody = null;
        try {
            requestBody =
                    new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart(
                                    "certificate",
                                    uploadedFile.getName(),
                                    RequestBody.create(
                                            MediaType.parse("application/x-pem-file; charset=utf-8"),
                                            uploadedFile.getBytes()))
                            .build();
        } catch (IOException e) {
            log.error("[Error][Failed in read uploaded certificate], Path={}", uploadedFile.getOriginalFilename(), e);
        }

        Request request = new Request.Builder().url(reqParam.getUrl()).post(requestBody).build();
        String result = getResult(request);
        return parseToDto(reqParam.getResDtoClz(), result, reqParam.getUrl());
    }

    private String getResult(Request request) throws OceanException {
        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                return response.body().string();
            }
            throw new OceanException(ErrorCode.CONNECTION_ERROR.getCode(), ErrorCode.CONNECTION_ERROR.getMessage());
        } catch (Exception e) {
            log.error("[Error][Executing Post request error], code={}, message={}",
                    ErrorCode.CONNECTION_ERROR.getCode(), ErrorCode.CONNECTION_ERROR.getMessage(), e);
            throw new OceanException(ErrorCode.CONNECTION_ERROR.getCode(), ErrorCode.CONNECTION_ERROR.getMessage());
        }
    }

    private Optional<Object> parseToDto(Class resDtoClz, String result, String apiUrl) {
        try {
            Object resDto = objectMapper.readValue(result, resDtoClz);
            return Optional.of(resDto);
        } catch (IOException e) {
            log.error("[Error][Parsing error], API request url={}, result={}, code={}, message={}",
                    apiUrl, result, ErrorCode.JSON_PARSE_ERROR.getCode(),
                    ErrorCode.JSON_PARSE_ERROR.getMessage(), e);
        }
        return Optional.empty();
    }
}
