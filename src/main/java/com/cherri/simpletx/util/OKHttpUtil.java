package com.cherri.simpletx.util;

import com.cherri.simpletx.model.enumerator.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class OKHttpUtil {

    public static final String PROTOCOL_VERSION = "TLSv1.2";
    public static final Integer MAX_REQUESTS = 128;
    public static final Integer MAX_REQUESTS_PER_HOST = 16;
    public static final long DEFAULT_CONNECT_TIMEOUT = 10;
    public static final long DEFAULT_READ_TIMEOUT = 15;

    public static String get(String url) throws IOException {
        OkHttpClient client = getUnsafeTls12OkHttpClient(DEFAULT_CONNECT_TIMEOUT, DEFAULT_READ_TIMEOUT);
        Request request = new Request.Builder().url(url).get().build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    private static OkHttpClient getUnsafeTls12OkHttpClient(Long connectTimeout, Long readTimeout) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts =
                    new TrustManager[]{
                            new X509TrustManager() {

                                @Override
                                public void checkClientTrusted(
                                        java.security.cert.X509Certificate[] chain, String authType)
                                        throws CertificateException {
                                }

                                @Override
                                public void checkServerTrusted(
                                        java.security.cert.X509Certificate[] chain, String authType)
                                        throws CertificateException {
                                }

                                @Override
                                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return new java.security.cert.X509Certificate[]{};
                                }
                            }
                    };

            SSLContext sc = SSLContext.getInstance(PROTOCOL_VERSION);
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sc.getSocketFactory();
            HostnameVerifier hostnameVerifier =
                    new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    };
            Dispatcher dispatcher = new Dispatcher();
            dispatcher.setMaxRequests(MAX_REQUESTS);
            dispatcher.setMaxRequestsPerHost(MAX_REQUESTS_PER_HOST);
            return new OkHttpClient.Builder()
                    .retryOnConnectionFailure(false)
                    .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                    .readTimeout(readTimeout, TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                    .hostnameVerifier(hostnameVerifier)
                    .dispatcher(dispatcher)
                    .build();
        } catch (Exception e) {
            log.error("[Error][Using OKHttpUtil failed], code={}, message={}", ErrorCode.FAILED.getCode(), ErrorCode.FAILED.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
