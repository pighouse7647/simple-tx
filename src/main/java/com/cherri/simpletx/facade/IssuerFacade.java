package com.cherri.simpletx.facade;

import com.cherri.simpletx.dto.IssuerDTO;
import com.cherri.simpletx.dto.IssuerQueryDTO;
import com.cherri.simpletx.exception.OceanException;
import com.cherri.simpletx.service.IssuerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class IssuerFacade {
    private final IssuerService service;


    public IssuerFacade(IssuerService service) {
        this.service = service;
    }

    public boolean add(IssuerDTO issuerDTO) throws OceanException {
        IssuerDTO result = service.add(issuerDTO);
        return result != null && result.getId() > 0;
    }

    public boolean update(IssuerDTO issuerDTO) {
        IssuerDTO result = service.update(issuerDTO);
        return result != null && result.getId() > 0;
    }

    public boolean delete(IssuerDTO issuerDTO) {
        return service.delete(issuerDTO);
    }

    public List<IssuerDTO> queryListByQueryDTO(IssuerQueryDTO issuerQueryDTO) {
        return service.queryListByQueryDTO(issuerQueryDTO);
    }
}
