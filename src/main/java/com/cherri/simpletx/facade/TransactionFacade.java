package com.cherri.simpletx.facade;

import com.cherri.simpletx.dto.*;
import com.cherri.simpletx.service.IssuerService;
import com.cherri.simpletx.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TransactionFacade {
    private final TransactionService service;
    private final IssuerService issuerService;

    public TransactionFacade(TransactionService service, IssuerService issuerService) {
        this.service = service;
        this.issuerService = issuerService;
    }

    public TransactionDTO pay(TransactionPayDTO payDTO) {
        long begin_trans_time = new Date().getTime();
        final IssuerQueryDTO queryDTO = new IssuerQueryDTO();
        queryDTO.setIssuer_id(payDTO.getIssuer_id());
        final TransactionDTO result = service.pay(payDTO);
        result.setStart_millis(begin_trans_time);
        long end_trans_time = new Date().getTime();
        result.setEnd_millis(end_trans_time);
        service.updateTimeRecord(result);
        return result;
    }

    public Optional<PagingResultsDTO<List<TransactionDTO>>> query(TransactionQueryDTO transactionQueryDTO) {
        return service.queryListByQueryDTO(transactionQueryDTO);

    }
}
