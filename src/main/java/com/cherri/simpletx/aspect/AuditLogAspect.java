package com.cherri.simpletx.aspect;

import com.cherri.simpletx.dto.ApiPagesResponse;
import com.cherri.simpletx.dto.ApiResponse;
import com.cherri.simpletx.util.AuditLogToStringStyle;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Aspect
@Component
@Log4j2
public class AuditLogAspect {

    public static final AuditLogToStringStyle AUDIT_LOG_STYLE = new AuditLogToStringStyle();

    private static final Set<Class<?>> WRAPPER_TYPES = getDefaultObjectTypes();
    @Autowired
    private HttpSession session;
    @Autowired
    private HttpServletRequest request;

    public static boolean isDefaultObjectType(Class<?> clazz) {
        return WRAPPER_TYPES.contains(clazz);
    }

    private static Set<Class<?>> getDefaultObjectTypes() {
        Set<Class<?>> ret = new HashSet<Class<?>>();
        ret.add(Boolean.class);
        ret.add(Character.class);
        ret.add(Byte.class);
        ret.add(Short.class);
        ret.add(Integer.class);
        ret.add(Long.class);
        ret.add(Float.class);
        ret.add(Double.class);
        ret.add(Void.class);
        ret.add(Array.class);
        ret.add(List.class);
        ret.add(ArrayList.class);
        ret.add(String.class);
        return ret;
    }

    @Around(value = "@annotation(com.cherri.simpletx.aspect.AuditLogHandler)")
    public Object auditLogHandle(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = signature.getMethod();
        AuditLogMethodNameEnum methodName = method.getAnnotation(AuditLogHandler.class).methodName();
        Object[] args = proceedingJoinPoint.getArgs();
//    String ip = IpUtils.getIPFromRequest(request);
        String value = generateParamString(args, signature.getParameterNames());
        Object returnObject = proceedingJoinPoint.proceed();
        int status = 1;
        if (returnObject != null) {
            if (returnObject instanceof ApiResponse) {
                status = ((ApiResponse) returnObject).getCode();
            } else if (returnObject instanceof ApiPagesResponse) {
                status = ((ApiPagesResponse) returnObject).getCode();
            } else {
                status = returnObject == null ? 500 : 0;
            }
        }
        String action = status == 0 ? "Y" : "N";
        String user = (String) session.getAttribute("account");
        user = user == null ? "test" : user;

        log.info(
                "Audit Log - methodName:{}, value:{}, action:{}, errorCode:{}, user:{}",
                methodName,
                value,
                action,
                status,
                user
        );
        return returnObject;
    }

    private String generateParamString(Object[] args, String[] parameterNames) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            if (i > 0) {
                sb.append("|");
            }
            if (isDefaultObjectType(args[i].getClass())) {
                sb.append(parameterNames[i] + "=" + args[i]);
            } else if (args[i] instanceof MultipartFile) {
                // multipart file do nothing
            } else if (args[i] != null) {
                sb.append(args[i]);
            }
        }
        return sb.toString();
    }
}
