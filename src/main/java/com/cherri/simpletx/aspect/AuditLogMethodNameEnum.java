package com.cherri.simpletx.aspect;

public enum AuditLogMethodNameEnum {

  /** Issuer */
  ISSUER_CREATE,
  ISSUER_LIST,
  ISSUER_UPDATE,
  ISSUER_DELETE,

  /** Transaction **/
  TRANSACTION_PAY,
  TRANSACTION_QUERY,

}
