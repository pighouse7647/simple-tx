package com.cherri.simpletx.service;

import com.cherri.simpletx.dto.*;
import com.cherri.simpletx.exception.OceanException;
import com.cherri.simpletx.model.dao.IssuerDAO;
import com.cherri.simpletx.model.dao.TransactionDAO;
import com.cherri.simpletx.model.entity.Transaction;
import com.cherri.simpletx.model.enumerator.ErrorCode;
import com.cherri.simpletx.util.InternalHttpConnector;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TransactionService {

    private final TransactionDAO dao;

    private final IssuerDAO issuerDAO;

    private final InternalHttpConnector connector;

    private final ObjectMapper mapper;

    @Value("${transaction.verify.url}")
    private String verifyUrl;

    public TransactionService(TransactionDAO dao, IssuerDAO issuerDAO, InternalHttpConnector connector, ObjectMapper mapper) {
        this.dao = dao;
        this.issuerDAO = issuerDAO;
        this.connector = connector;
        this.mapper = mapper;
    }

    public TransactionDTO pay(TransactionPayDTO payDTO) {
        if (!issuerDAO.getById(payDTO.getIssuer_id()).isPresent()) {
            throw new OceanException(ErrorCode.ISSUER_NOT_FOUND.getCode(), ErrorCode.ISSUER_NOT_FOUND.getMessage());
        }
        if (!verify(payDTO)) {
            throw new OceanException(ErrorCode.FAILED.getCode(), ErrorCode.FAILED.getMessage());
        }

        final Transaction transaction = new Transaction();
        transaction.setIssuerId(payDTO.getIssuer_id());
        transaction.setMaskedAcctNumber(this.getMaskAcctNumber(payDTO.getAcct_number()));
        transaction.setAmount(payDTO.getAmount());
        transaction.setTransId(UUID.randomUUID().toString());
        final Transaction result = dao.pay(transaction);
        final TransactionDTO dto = new TransactionDTO();
        dto.setTrans_id(result.getTransId());
        dto.setIssuer_id(result.getIssuerId());
        dto.setAmount(result.getAmount());
        dto.setStart_millis(result.getStartMillis());
        dto.setEnd_millis(result.getEndMillis());
        dto.setId(result.getId());
        dto.setAcct_number(result.getMaskedAcctNumber());
        return dto;
    }

    /**
     * 遮蔽後的卡號(如卡號為4242424242424242, 請儲存424242******4242)
     *
     * @param originalAcctNumber
     * @return
     */
    private String getMaskAcctNumber(final String originalAcctNumber) {
        final String maskRegex = "([0-9]{6})([0-9]){6}([0-9]{4})";
        final String replacementPattern = "$1******$3";
        return originalAcctNumber.replaceAll(maskRegex, replacementPattern);
    }


    public Optional<PagingResultsDTO<List<TransactionDTO>>> queryListByQueryDTO(final TransactionQueryDTO queryDTO) {
        final Optional<Page<Transaction>> opt = dao.queryListByQueryDTO(queryDTO);
        if (opt.isPresent()) {
            final Page<Transaction> page = opt.get();
            final List<TransactionDTO> dtoData =
                    page.getContent().stream()
                            .map(TransactionDTO::valueOf)
                            .collect(Collectors.toList());

            final PagingResultsDTO result = PagingResultsDTO.valueOf(page);
            result.setRecords_per_page(queryDTO.getRecords_per_page());
            result.setData(dtoData);
            return Optional.of(result);
        }
        log.error("[findTransaction] error.");
        return Optional.empty();
    }

    public boolean verify(final TransactionPayDTO payDTO) {
        final InternalHttpRequestParameter params = new InternalHttpRequestParameter();
        params.setUrl(verifyUrl);
        params.setResDtoClz(TransactionVerifyDTO.class);
        try {
            final TransactionVerifyDTO value = new TransactionVerifyDTO();
            value.setPass_code(payDTO.getPasscode());
            params.setReqJson(mapper.writeValueAsString(value));
        } catch (JsonProcessingException e) {
            log.error("[Error][Verify transaction pay], code={}, message={}", ErrorCode.JSON_PARSE_ERROR.getCode(),
                    ErrorCode.JSON_PARSE_ERROR.getMessage());
            throw new OceanException(ErrorCode.JSON_PARSE_ERROR.getCode(), ErrorCode.JSON_PARSE_ERROR.getMessage());
        }

        Optional<Object> post = connector.post(params);
        if (!post.isPresent()) {
            log.error("[Error][Verify transaction pay], code={}, message={}", ErrorCode.CONNECTION_ERROR.getCode(),
                    ErrorCode.CONNECTION_ERROR.getMessage());
            throw new OceanException(ErrorCode.CONNECTION_ERROR.getCode(), ErrorCode.CONNECTION_ERROR.getMessage());
        }

        return ((TransactionVerifyDTO) post.get()).isVerified();
    }

    public boolean updateTimeRecord(TransactionDTO dto) {
        final Transaction t = new Transaction();
        t.setId(dto.getId());
        t.setStartMillis(dto.getStart_millis());
        t.setEndMillis(dto.getEnd_millis());
        return dao.updateTimeRecord(t);
    }
}
