package com.cherri.simpletx.service;

import com.cherri.simpletx.dto.IssuerDTO;
import com.cherri.simpletx.dto.IssuerQueryDTO;
import com.cherri.simpletx.exception.OceanException;
import com.cherri.simpletx.model.dao.IssuerDAO;
import com.cherri.simpletx.model.entity.Issuer;
import com.cherri.simpletx.model.enumerator.ErrorCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class IssuerService {
    private final IssuerDAO dao;
    private ObjectMapper objectMapper;

    public IssuerService(IssuerDAO dao, ObjectMapper objectMapper) {
        this.dao = dao;
        this.objectMapper = objectMapper;
    }

    public IssuerDTO add(IssuerDTO issuerDTO) throws OceanException {
        if (issuerDTO == null) {
            throw new OceanException(ErrorCode.EXCEPTION_HAPPENED);
        }
        // 已刪除的視為不存在
        if (!CollectionUtils.isEmpty(dao.getByBankCode(issuerDTO.getBank_code()))) {
            throw new OceanException(ErrorCode.BANK_CODE_DUPLICATED);
        }

        Issuer issuer = new Issuer();
        issuer.setName(issuerDTO.getName());
        issuer.setBankCode(issuerDTO.getBank_code());

        Issuer result = dao.add(issuer);
        issuerDTO.setId(result.getId());
        return issuerDTO;
    }


    public IssuerDTO update(IssuerDTO issuerDTO) {
        final Issuer issuer = new Issuer();
        issuer.setId(issuerDTO.getIssuer_id());
        issuer.setName(issuerDTO.getName());
        issuer.setBankCode(issuerDTO.getBank_code());

        final Issuer result = dao.update(issuer);
        issuerDTO.setId(result.getId());
        return issuerDTO;
    }

    public boolean delete(IssuerDTO issuerDTO) {
        return dao.delete(issuerDTO.getIssuer_id());
    }

    public List<IssuerDTO> queryListByQueryDTO(IssuerQueryDTO issuerQueryDTO) {

        return dao.getListByQueryDTO(issuerQueryDTO).stream()
                .map(issuer -> new IssuerDTO(issuer.getId(), issuer.getName(),
                        issuer.getBankCode(), issuer.getCreateMills(), issuer.getUpdateMills(),
                        issuer.getDeleteFlag(), issuer.getId(),
                        null))
                .collect(Collectors.toList());
    }
}
