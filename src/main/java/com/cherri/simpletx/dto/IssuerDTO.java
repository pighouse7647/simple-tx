package com.cherri.simpletx.dto;

import com.cherri.simpletx.model.entity.Transaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IssuerDTO {

    private Long id;

    private String name;

    private String bank_code;

    private Long create_mills;

    private Long update_mills;

    private Boolean delete_flag;

    private Long issuer_id;

    private List<Transaction> transactions = new ArrayList<>();
}
