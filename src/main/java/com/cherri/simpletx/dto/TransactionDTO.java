package com.cherri.simpletx.dto;

import com.cherri.simpletx.model.entity.Transaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Transient;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDTO {
    private Long id;

//    private Issuer issuer;

    private String trans_id;

    private String acct_number;

    private Integer amount;

    private Long start_millis;

    private Long end_millis;

    @Transient
    private Long issuer_id;

    public static TransactionDTO valueOf(Transaction transaction) {
        return new TransactionDTO(transaction.getId(),
//                transaction.getIssuer(),
                transaction.getTransId(),
                transaction.getMaskedAcctNumber(), transaction.getAmount(), transaction.getStartMillis(),
                transaction.getEndMillis(),
//                transaction.getIssuer().getId()
                transaction.getIssuerId()
        );

    }
}
