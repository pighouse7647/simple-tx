package com.cherri.simpletx.dto;

import lombok.Data;

@Data
public class IssuerQueryDTO {

    private Long issuer_id;

}
