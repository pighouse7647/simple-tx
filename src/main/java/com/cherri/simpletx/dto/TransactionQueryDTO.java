package com.cherri.simpletx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 交易查詢物件
 */
@Data
public class TransactionQueryDTO {

    @ApiModelProperty(notes = "Records per page", example = "10", required = true)
    private Integer records_per_page;

    @ApiModelProperty(notes = "Current page", example = "1", required = true)
    private Integer page;

    @ApiModelProperty(notes = "Issuer's Id", hidden = true)
    private Long issuer_id;

    @ApiModelProperty(notes = "Transaction Id (UUID)", hidden = true)
    private String trans_id;

    @ApiModelProperty(notes = "Amount", hidden = true)
    private Amount amount;

}
