package com.cherri.simpletx.dto;

import com.cherri.simpletx.model.enumerator.ResultStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 為了方便 扁平化(移除繼承)
 * @param <T>
 */
@Data
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiPagesResponse<T> {
    private Integer code;
    private String message;
    private PagingResultsDTO page;
    private T data;


    /**
     * 預設有回傳值的成功狀態
     *
     * @param pagingResult
     */
    public ApiPagesResponse(PagingResultsDTO<T> pagingResult) {
        setData(pagingResult.getData());
        this.page = new PagingResultsDTO(pagingResult.getRecords_per_page(), pagingResult.getTotal_elements(), pagingResult.getTotal_pages(), pagingResult.getNumber(), null);
        this.code = ResultStatus.SUCCESS.getCode();
        this.message = "success";
    }

    /**
     * 自定義錯誤訊息
     *
     * @param status
     * @param message
     */
    public ApiPagesResponse(ResultStatus status, String message) {
        this.code = status.getCode();
        this.message = message;
    }
}
