package com.cherri.simpletx.dto;

import com.cherri.simpletx.aspect.AuditLogAspect;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class PageQueryDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  @NotNull(message = "{column.notempty}")
  private Integer page = 1;

  private Integer pageSize = 20;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, AuditLogAspect.AUDIT_LOG_STYLE);
  }
}
