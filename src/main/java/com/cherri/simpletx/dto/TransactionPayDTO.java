package com.cherri.simpletx.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
public class TransactionPayDTO {

    @NotNull
    @ApiModelProperty(notes = "Issuer's Id", example = "21", required = true)
    private Long issuer_id;

    @NotBlank
    @ApiModelProperty(notes = "Card Number", example = "4242424242424242", required = true)
    private String acct_number;

    @NotBlank
    @ApiModelProperty(notes = "External verify api", example = "123456", required = true)
    private String passcode;

    @Positive
    @ApiModelProperty(notes = "Amount", example = "199887766", required = true)
    private Integer amount;


}
