package com.cherri.simpletx.dto;

import com.cherri.simpletx.model.enumerator.ResultStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiIssuerPagesResponse<T> extends ApiPagesResponse<T> {
    private Integer status;
    private String message;
    private PagingResultsDTO page;
    private T transaction_list;

    @Override
    @JsonIgnore
    public T getData() {
        return super.getData();
    }


    /**
     * 預設有回傳值的成功狀態
     *
     * @param pagingResult
     */
    public ApiIssuerPagesResponse(PagingResultsDTO<T> pagingResult) {
      super(pagingResult);
        this.page = new PagingResultsDTO(pagingResult.getRecords_per_page(), pagingResult.getTotal_elements(), pagingResult.getTotal_pages(), pagingResult.getNumber(), null);
        this.status = ResultStatus.SUCCESS.getCode();
        this.message = "success";
      this.transaction_list = getData();
    }

    public ApiIssuerPagesResponse() {
        super();
        this.transaction_list = getData();
    }

    /**
     * 自定義錯誤訊息
     *
     * @param status
     * @param message
     */
    public ApiIssuerPagesResponse(ResultStatus status, String message) {
        this.status = status.getCode();
        this.message = message;
    }
}
