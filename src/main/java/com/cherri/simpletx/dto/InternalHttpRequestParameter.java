package com.cherri.simpletx.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InternalHttpRequestParameter {

  private String reqJson;

  private String url;

  private Class resDtoClz;
}
