package com.cherri.simpletx.dto;

import com.cherri.simpletx.model.enumerator.ErrorCode;
import com.cherri.simpletx.model.enumerator.ResultStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse<T> {

    public static final ApiResponse SUCCESS_API_RESPONSE = new ApiResponse();

    @JsonInclude(JsonInclude.Include.USE_DEFAULTS)
    private T data;

    private Integer code;

    private String message;

    /**
     * 只回傳status和message
     */
    public ApiResponse() {
        this.code = 0;
        this.message = "success";
    }

    /**
     * 預設有回傳值的成功狀態
     *
     * @param data
     */
    public ApiResponse(T data) {
        this.data = data;
        this.code = 0;
        this.message = "success";
    }

    /**
     * 自定義錯誤訊息
     *
     * @param status
     * @param message
     */
    public ApiResponse(ResultStatus status, String message) {
        this.code = status.getCode();
        this.message = message;
    }

    public ApiResponse(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
    }

    public static ApiResponse valueOf(OperationResult result) {
        return new ApiResponse(result.getStatus(), result.getMessage());
    }

    public static <T> ApiResponse valueOf(T data) {
        return new ApiResponse(data);
    }

    public static ApiResponse createFailInstance(ResultStatus status) {
        return new ApiResponse(status, "Fail");
    }
}
