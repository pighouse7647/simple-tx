package com.cherri.simpletx.dto;

import com.cherri.simpletx.model.enumerator.ResultStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OperationResult {

  private static final OperationResult SUCCESS =
      new OperationResult(ResultStatus.SUCCESS, "success");

  private ResultStatus status;
  private String message;

  public boolean isSuccess() {
    return ResultStatus.SUCCESS.equals(status);
  }

  public static OperationResult getSuccessInstance() {
    return OperationResult.SUCCESS;
  }
}
