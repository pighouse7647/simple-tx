package com.cherri.simpletx.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.domain.Page;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PagingResultsDTO<T> {
  private Integer records_per_page = 1;
  private Long total_elements = 0L;
  private Integer total_pages = 0;
  private Integer number = 0;
  private T data;

    public static <T> PagingResultsDTO<T> valueOf(Page page) {
    if(page == null){
      return null;
    }
    int currentPage = page.getNumber() + 1;
    return new PagingResultsDTO(10,
            page.getTotalElements(), page.getTotalPages(), currentPage, page.getContent());
  }
}
