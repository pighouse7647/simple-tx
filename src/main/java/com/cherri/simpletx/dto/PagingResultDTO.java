package com.cherri.simpletx.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.domain.Page;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PagingResultDTO<T> {
  private Long total = 0L;
  private Integer totalPages = 0;
  private Integer currentPage = 1;

  @JsonInclude(JsonInclude.Include.USE_DEFAULTS)
  private T data;

  public PagingResultDTO(T data) {
    setData(data);
  }

  public static <T> PagingResultDTO<T> valueOf(Page page) {
    if(page == null){
      return null;
    }
    int currentPage = page.getNumber() + 1;
    return new PagingResultDTO(
        page.getTotalElements(), page.getTotalPages(), currentPage, page.getContent());
  }
}
