package com.cherri.simpletx.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

/**
 * 為了方便 扁平化(移除繼承)
 * 真正目的在於客製化回傳格式(移除data這層)
 * @param <T>
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiTransactionPayResponse<T extends TransactionDTO> extends ApiResponse<T> {

    public static final ApiTransactionPayResponse SUCCESS_API_RESPONSE = new ApiTransactionPayResponse();

    private String trans_id;

    private Long issuer_id;

    private Integer amount;

    private Long start_millis;

    private Long end_millis;


    @JsonIgnore
    public T getData() {
        return super.getData();
    }

    /**
     * 只回傳status和message
     */
    public ApiTransactionPayResponse() {
        super();
    }

    /**
     * 預設有回傳值的成功狀態
     *
     * @param data
     */
    public ApiTransactionPayResponse(T data) {
        super(data);
        this.trans_id = data.getTrans_id();
        this.issuer_id = data.getIssuer_id();
        this.amount = data.getAmount();
        this.start_millis = data.getStart_millis();
        this.end_millis = data.getEnd_millis();
    }

}
