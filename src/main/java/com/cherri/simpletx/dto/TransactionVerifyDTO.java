package com.cherri.simpletx.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 外部介接（交易Verify）用
 */
@Data
@NoArgsConstructor
public class TransactionVerifyDTO {
    
    private String pass_code;
    private boolean verified;


}
