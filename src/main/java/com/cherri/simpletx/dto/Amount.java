package com.cherri.simpletx.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Amount {
    private Integer minimum;
    private Integer maximum;
}
