package com.cherri.simpletx.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiIssuerResponse<T> extends ApiResponse<T> {

    public static final ApiIssuerResponse SUCCESS_API_RESPONSE = new ApiIssuerResponse();

    @JsonInclude(JsonInclude.Include.USE_DEFAULTS)
    @JsonIgnoreProperties({"issuer_id", "create_mills", "update_mills", "delete_flag", "transactions"})
    private T issuer_list;

    @Override
    @JsonIgnore
    public T getData() {
        return super.getData();
    }

    public ApiIssuerResponse(T issuer_list) {
        super(issuer_list);
        this.issuer_list = getData();
    }

    /**
     * 只回傳status和message
     */
    public ApiIssuerResponse() {
        super();
        this.issuer_list = getData();
    }


    public static <T> ApiIssuerResponse valueOf(T data) {
        return new ApiIssuerResponse(data);
    }

}
