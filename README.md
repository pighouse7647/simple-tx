## 訓練專案

### Entity
- Issuer (as one)

- Transaction (as Many)


### API
- CRUD for Issuer.
- Pay, Query(with pagination) for Transaction.



### Dependency
- SpringBoot 2.1.8.RELEASE
- okhttp3 3.14.0
- lombok
- Swagger2, Swagger UI
- Oracle DB(Docker Edition)